//
//  CMandopLocationVC.swift
//  Astlem Orders
//
//  Created by Ahmed Adm on 08/08/2020.
//  Copyright © 2020 Ahmed Adm. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import SwiftSignalRClient



class CMandopLocationVC: UIViewController , CLLocationManagerDelegate ,GMSMapViewDelegate {

    @IBOutlet var google_map: GMSMapView!
    @IBOutlet var isOnlileSwitch:UISwitch!
    private var locationManager:CLLocationManager?
    private var local_location: CLLocation!

    /* * * * * * * * * CHANGE PHONE NUMBER OF USER * * * * * * * * * * * * * */
    private var current_user_id:String = "0536301031"// MARK: change the phone number
    
    private let serverUrl = "http://astalem.com/orderHubs"
    private var chatHubConnection: HubConnection?
    private var chatHubConnectionDelegate: HubConnectionDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.google_map.delegate = self
        
        // zoom
        let camera = GMSCameraPosition.camera(withLatitude: Double(24.756752), longitude: Double(46.738410), zoom: 17.0)
        self.google_map.camera = camera
        self.google_map.isMyLocationEnabled = true

        locationManager = CLLocationManager()
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
        locationManager?.allowsBackgroundLocationUpdates = false

        self.connectToServer()
        
    }
    
    @IBAction func online_switch(_ sender: UISwitch){
        self.setActive(isOn: sender.isOn)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         
         if let location = locations.last{
             let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 17.0)
             //self.google_map.camera = camera
             self.google_map.animate(to: camera)
         }
        
     }
    
    func connectToServer(){
        
        self.chatHubConnectionDelegate = ChatHubConnectionDelegate(controller: self)
        
        self.chatHubConnection = HubConnectionBuilder(url: URL(string: self.serverUrl)!)
            .withLogging(minLogLevel: .debug)
            .withAutoReconnect()
            .withHubConnectionDelegate(delegate: self.chatHubConnectionDelegate!)
            .build()
        self.chatHubConnection!.start()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.connectToServer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.locationManager?.stopUpdatingLocation()
        chatHubConnection?.stop()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func get_order_details(orderId:Int){
        
        var api = "http://astalem.com/api/Order/ProviderGetOrderDetails?orderId=\(orderId)"
                
        let url = URL(string: api )
        var request : URLRequest = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*;", forHTTPHeaderField: "Accept")
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data,
            let response = response as? HTTPURLResponse,
            error == nil else { // check for fundamental networking error
                print("error", error ?? "Unknown error")
                return
            }
            
            guard (200 ... 299) ~= response.statusCode else {// check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            if let responseString = String(data: data, encoding: .utf8) {
                print("server response : \(responseString)")
            }
            
            do{
                
                let orderDetails:OrderDetails = try JSONDecoder().decode(OrderDetails.self, from: data)
                
                DispatchQueue.main.async {
                    // go to next page
                    var vc = self.storyboard?.instantiateViewController(withIdentifier: "order_details") as? OrderMandopDetailsVC
                    vc?.orderDetails = orderDetails
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
                
                
            }catch{
                print("error in load details")
            }
            
        }
        
        task.resume()


    }
    
    func setActive(isOn:Bool){
        
        var api = "http://astalem.com/api/Provider/SetActive?providerPhone=\(self.current_user_id)"
        
        if isOn == false{
            api = "http://astalem.com/api/Provider/SetNotActive?providerPhone=\(self.current_user_id)"
        }
        
        let url = URL(string: api )
        var request : URLRequest = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*;", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data,
            let response = response as? HTTPURLResponse,
            error == nil else { // check for fundamental networking error
                print("error", error ?? "Unknown error")
                return
            }
            
            guard (200 ... 299) ~= response.statusCode else {// check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            if let responseString = String(data: data, encoding: .utf8) {
                print("server response : \(responseString)")
            }
            
        }
        
        task.resume()
        
    }
    
    // these functions for SignalR delegate
    // you have nothing to do here in these functions
    fileprivate func connectionDidOpen() {
        //toggleUI(isEnabled: true)
        // save the user id to this connection
        chatHubConnection?.invoke(method: "SaveProviderConnectionId", self.current_user_id  ){ error in
            if let error = error {
                print("error in SaveProviderConnectionId : \(error)")
            }
        }
        
        // Message, otherUserId, DateTimeNow, MessageType
        self.chatHubConnection!.on(method: "ReceiveOrder", callback: {( orderId: Int) in
            print("we get message: \(orderId)")
            // i case the user are online he will get to next page for order details
            if self.isOnlileSwitch.isOn {
                self.get_order_details(orderId: orderId)
            }
            
        })
        
    }
        
    fileprivate func connectionDidFailToOpen(error: Error) {

    }

    fileprivate func connectionDidClose(error: Error?) {

    }

    fileprivate func connectionWillReconnect(error: Error?) {

    }

    fileprivate func connectionDidReconnect() {

    }

}



class ChatHubConnectionDelegate: HubConnectionDelegate {

    weak var controller: CMandopLocationVC?

    init(controller: CMandopLocationVC) {
        self.controller = controller
    }

    func connectionDidOpen(hubConnection: HubConnection) {
        controller?.connectionDidOpen()
    }

    func connectionDidFailToOpen(error: Error) {
        controller?.connectionDidFailToOpen(error: error)
    }

    func connectionDidClose(error: Error?) {
        controller?.connectionDidClose(error: error)
    }

    func connectionWillReconnect(error: Error) {
        controller?.connectionWillReconnect(error: error)
    }

    func connectionDidReconnect() {
        controller?.connectionDidReconnect()
    }
}
