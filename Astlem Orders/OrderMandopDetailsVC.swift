//
//  OrderMandopDetailsVC.swift
//  Astlem Orders
//
//  Created by Ahmed Adm on 08/08/2020.
//  Copyright © 2020 Ahmed Adm. All rights reserved.
//

import UIKit
import GoogleMaps

struct OrderDetails: Decodable {
    var orderId:Int!
    var descriptionLocation:String!
    var descriptionOrder:String!
    var clientPhone:String!
    var clientImage:String!
    var distanceOrderLocationFromClientLocation:Int!
    var userLat:Int!
    var orderLat:Int!
    var orderLong:Int!
    var userLong:Int!
}

class OrderMandopDetailsVC: UIViewController , GMSMapViewDelegate , UITextFieldDelegate {
    
    var orderDetails:OrderDetails!
    @IBOutlet weak var google_map: GMSMapView!
    @IBOutlet weak var customer_distance: UILabel!
    @IBOutlet weak var your_distance: UILabel!
    @IBOutlet weak var offer_price: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.google_map.delegate = self
        self.addMarkers()
        
        
        
        // this to hide keyboard
        self.offer_price.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismisskeyboard))
        self.view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        
    }
    
    deinit{
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }
    
    
    func addMarkers(){
        
        DispatchQueue.main.async {
            
            // clear marks
            self.google_map.clear()
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: Double(self.orderDetails.orderLat!) , longitude: Double(self.orderDetails.orderLong!) )
            marker.title = self.orderDetails.descriptionLocation!
            marker.snippet = self.orderDetails.descriptionOrder!
            //marker.icon = UIImage(named: "taxi_car")
            marker.map = self.google_map
            marker.userData = self.orderDetails // here we sent the data to marker
            
            
            let camera = GMSCameraPosition.camera(withLatitude: Double(self.orderDetails.orderLat!), longitude: Double(self.orderDetails.orderLong!), zoom: 17.0)
            self.google_map.camera = camera
            
            
        }
        
    }
    
    
    @IBAction func reject(_ sender: Any) {
        // TODO: here we have to complete the task
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accept(_ sender: Any) {
        // TODO: here we have to complete the task
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // keyboard functions
    @objc func keyboardWillChange(notification : Notification){
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if notification.name == UIResponder.keyboardWillShowNotification ||
            notification.name == UIResponder.keyboardWillChangeFrameNotification {
            view.frame.origin.y = -(keyboardRect.height - 80)
        }else{
            view.frame.origin.y = 0
        }
        
        
    }
    
    @objc func dismisskeyboard(){
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //textField.resignFirstResponder()
        self.view.endEditing(true)
        return false
    }
    
    
}
